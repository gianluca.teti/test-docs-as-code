#! /bin/bash
# Run the Docs as Code builder scripts using Docker

$image=$(Get-Content .image-name.txt)

docker container rm -f mySphinx
docker container run -it --rm -p 8080:80 --mount "type=bind,src=$PWD,target=/docs-as-code" --rm --name mySphinx $image $ARGS
