.. index::
    single: Role of the Technical Writer

========================
Role of the Tech Writer
========================

So if the rest of the development team is writing the content for us
what is left for the technical writer to do?

Well quite a lot of course. For example:

* Planning/coordination
* Design
    * Templates
    * Style guides
    * Vocabulary lists
    * Information typing and topic maps
    * UX Design
    * Create and manage the documentation Information Architecture
    * ...
* Quality assurance and Verification
* Training and education
* Content Refinement
* Process improvement
* Scalability

.. <!-- alex ignore dirty -->

So there is obviously a lot of high value the technical writer can provide once they
can be freed from the minutia. However there will always be the need to get the
"hands dirty" by being the proof reader of last resort!
