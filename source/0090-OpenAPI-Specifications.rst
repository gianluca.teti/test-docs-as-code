.. index::
    single: OpenAPI Specifications

======================
OpenAPI Specifications
======================

Presenting `OpenAPI Specification (OAS) <https://oai.github.io/Documentation/specification.html>`_
content in an existing docs website can take some effort.

Note: This document does not explain what, or how to use, an OAS document. We assume that you already have a OAS yaml or
JSON file that you need to integrate into your docs.

The tools available to render OAS (for example
`OpenAPI Generator <https://openapi-generator.tech/>`_, 
`Redoc <https://github.com/Redocly/redoc>`_, or
`Swagger UI <https://swagger.io/tools/swagger-ui/>`_ )
create standalone content that is not designed to
be easily integrated into an existing website theme.

This can present some challenges for site navigation (e.g. loss of breadcrumbs and other navigational cues)
and may make the consumer experience jarring.

There are a variety of tools design to help.
For instance Sphinx has the `sphinxcontrib-openapi <https://sphinxcontrib-openapi.readthedocs.io/>`_ module. 

.. openapi:: _static/openapi.yaml
