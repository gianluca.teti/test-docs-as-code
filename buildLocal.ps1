#!/usr/bin/env sh

$image=$(Get-Content .image-name.txt)

docker image build -t $image docker-build-context
